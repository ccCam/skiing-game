extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	add_highscores()

func add_highscores():
	var highscores = get_node("/root/Global").highscores
	var highscores_text = "[center]"
	
	if highscores.size() == 0:
		highscores_text = "No Highscores"
	else:
		for highscore in highscores:
			highscores_text += str(highscore[0]) + ": " + str(highscore[1]) + "\n"
		
	highscores_text += "[/center]"
	print(highscores_text)
	get_tree().get_root().get_node("Control/rlbl_highscores").set_bbcode(highscores_text)
	
func _on_btn_back_pressed():
	get_tree().change_scene("res://Scenes/MainMenu.tscn")
