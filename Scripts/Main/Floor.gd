extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

#onready var main = get_node("/root/Node")
var speed = 1000
var vel = Vector2()

func _ready():
	set_process(true)
	#speed = main.game_speed
	pass

func _process(delta):
	vel = Vector2(0, -speed)
	
	if (get_global_position().y <= -1028):
		set_global_position(Vector2(get_global_position().x, 0))
	
	set_global_position(get_global_position() + vel * delta)
	


func _on_ScoreLabel_speed_up():
	print("Speedsup")
	speed += 100
