extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
	

func died():
	var score = get_tree().get_root().get_node("Node/HUD/ScoreLabel").score
	var lbl_score = self.get_node("lbl_score")
	print(score)
	lbl_score.text = ("You Scored: " + str(score))
	check_highscore()

func check_highscore():
	var highscores = get_node("/root/Global").highscores
	print(highscores)
	if highscores.size() < 10:
		print("Less than 10")
		#Show the "add highscore" box here
		add_highscore()
	else:
		var LOWEST_HIGHSCORE = highscores[highscores.size() - 1]
		print(LOWEST_HIGHSCORE)


func add_highscore():
	get_tree().get_root().get_node("Node/DeathControl/Buttons").hide()

func show_death_menu():
	get_tree().get_root().get_node("Node/DeathControl/Buttons").show()
	get_tree().get_root().get_node("Node/DeathControl/Highscore").hide()

func _on_btn_menu_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/MainMenu.tscn")


func _on_btn_restart_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/Main.tscn")


func _on_btn_quit_pressed():
	get_tree().quit()


func _on_btn_submit_pressed():
	var score = get_tree().get_root().get_node("Node/HUD/ScoreLabel").score
	var name = get_tree().get_root().get_node("Node/DeathControl/Highscore/TextEdit").text
	
	print(get_node("/root/Global").highscores)
	var highscores = get_node("/root/Global").highscores
	if highscores.size() == 10:
		highscores.remove(highscores.size() - 1)
		
	highscores.append([name, score])
	get_node("/root/Global").highscores = highscores
	print(get_node("/root/Global").highscores)
	get_node("/root/Global").save_highscores()
	
	show_death_menu()


func _on_btn_dont_submit_pressed():
	show_death_menu()
