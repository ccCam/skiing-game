extends Label

var score = 0

signal speed_up
signal add_obstacle

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass




func _on_KinematicBody2D_obstacle_passed():
	score += 1
	self.set_text(str(score))
	
	if score % 5 == 0:
		#Speedup time!
		print("This happened")
		emit_signal("speed_up")
		
	if score == 10:
		print("Obstacle added")
		emit_signal("add_obstacle")
