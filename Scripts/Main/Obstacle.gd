extends KinematicBody2D

var speed = 1000
var vel = Vector2()

signal obstacle_passed

func _ready():
	set_process(true)
	#speed = main.game_speed
	pass


func _process(delta):
	vel = Vector2(0, -speed)
	
	if (get_global_position().y <= -512):
		#set_global_position(Vector2(get_global_position().x, 1028))
		position_reset()
		print(speed)
	
	#set_global_position(get_global_position() + vel * delta)
	move_and_slide(vel)

func position_reset():
	set_global_position(Vector2(randi()%720, 1512))
	emit_signal("obstacle_passed")
	var sprite: Sprite =  self.get_child(0)
	sprite.region_enabled = true
	sprite.region_rect = rand_sprite()
	
func rand_sprite():
	if floor(rand_range(1, 3)) == 2:
		return Rect2(128, 0, 128, 128)
	else:
		#If it breaks, just return something we know exists
		return Rect2(128, 0, 128, 128)

	
	


func _on_Area2D_body_entered(body):
	if body.name == "Player":
		#Kill player here
		print("Player entered")
		
		var DeathControl: Control = get_tree().get_root().get_node("Node/DeathControl")
		var HUD: Control = get_tree().get_root().get_node("Node/HUD")
		
		DeathControl.show()
		DeathControl.died()
		
		HUD.hide()
		get_tree().paused = true


func _on_ScoreLabel_speed_up():
	print("Speedup")
	speed += 100
